package menu.resources;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Circle {

    private int circlePoint_X;
    private int circlePoint_Y;
    private int radiusCircle;

}



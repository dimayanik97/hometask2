package menu;

import java.util.Scanner;

public class Menu {

    MenuItem[] items;
    Scanner scanner;

    public Menu(Scanner scanner,MenuItem[] items) {

        this.scanner = scanner;
        this.items = items;

    }

    public void run() {

        while (true) {

            showMenu();

            int choice = getUserChoice();

            if (choice < 0 || choice >= items.length) {
                System.out.println("Вы ввели неверный ответ!");
                continue;
            }

            items[choice].execute();

            if (items[choice].isFinal()) break;

        }
    }

    private int getUserChoice() {

        System.out.print("Введите ответ: ");

        int ch = scanner.nextInt();
        scanner.nextLine();

        return ch-1;

    }

    private void showMenu() {

        System.out.println("------------------------------------------");

        for (int i = 0; i < items.length; i++) {
            System.out.printf("%2d - %s\n", i+1,items[i].getName());
        }

        System.out.println("------------------------------------------");

    }
}

package menu.items;

import menu.Main;
import menu.MenuItem;
import menu.resources.Point;
import java.util.List;

public class ShowDotsOutCircle extends Main implements MenuItem {

    @Override
    public String getName() {
        return "Показать точки вне окружности";
    }

    @Override
    public void execute() {

        List<Point> pointArray = pointList.getPoints();

        String result = "";

        System.out.println("------------------------------------------");

        if (pointArray.toArray().length < 1) {

            System.out.println("Вы не добавили точек!");

            if (circle.getRadiusCircle() == 0) {

                System.out.println("Вы не добавили окружность!");

            }

        } else {

            for (int a = 0; a < pointArray.toArray().length; a++) {

                double circlePointResult = Math.sqrt((pointArray.get(a).getPointX() - circle.getCirclePoint_X()) * 2 +
                        (pointArray.get(a).getPointY() - circle.getCirclePoint_Y()) * 2);

                if (circlePointResult > circle.getRadiusCircle()) {

                    result += (a + 1) + "-я точка не лежит в окружности!\n";

                }
            }

            System.out.print(result);

        }
    }
}

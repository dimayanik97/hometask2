package menu.items;

import menu.MenuItem;

public class ExitMenu implements MenuItem {

    @Override
    public String getName() {
        return "Завершить работу";
    }

    @Override
    public void execute() {

        System.out.println("------------------------------------------");
        System.out.println("Конец программы!");
        System.out.println("------------------------------------------");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}

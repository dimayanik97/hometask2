package menu.items;

import menu.Main;
import menu.MenuItem;
import java.util.Scanner;

public class ChangeCircleParameters extends Main implements MenuItem {

    @Override
    public String getName() {

        return "Изменение параметров окружности";

    }

    @Override
    public void execute() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("------------------------------------------");

        System.out.print("Введите координаты центра окружности:\nX = ");
        circle.setCirclePoint_X(scanner.nextInt());

        System.out.print("Y = ");
        circle.setCirclePoint_Y(scanner.nextInt());

        System.out.print("Введите длину радиуса окружности:\nЗначение = ");
        circle.setRadiusCircle(scanner.nextInt());

        System.out.println("------------------------------------------");
        System.out.println("Данные окружности успешно измененны!");

    }
}

package menu.items;

import menu.Main;
import menu.MenuItem;
import menu.resources.Point;
import java.util.Scanner;

public class AddNewDot extends Main implements MenuItem {

    @Override
    public String getName() {
        return "Добавление новой точки";
    }

    @Override
    public void execute() {

        System.out.println("------------------------------------------");
        Scanner scanner = new Scanner(System.in);

        int indexPoint = 0;

        do {
            int temp = 1;

            if (indexPoint >= 1) {
                System.out.print("Хотите создать еще одну точку? (Да = 1 | Нет = 2)\nОтвет: ");
                temp = scanner.nextInt();
            }

            if (temp == 1) {
                indexPoint++;
                Point point = new Point(0, 0);
                System.out.print("Введите координаты " + indexPoint + "-й(ой) точки:\nX = ");
                point.setPointX(scanner.nextInt());
                System.out.print("Y = ");
                point.setPointY(scanner.nextInt());
                pointList.updatePoints(point);

            } else if (temp > 2 || temp <= 0) {

                System.out.println("------------------------------------------");
                System.out.print("Вы ввели неправильный ответ!\n");
                System.out.println("------------------------------------------");

            } else {

                break;

            }

        } while (true);

        System.out.println("------------------------------------------");
        System.out.println("Точки успешно добавлены!");

    }
}

package menu;

import menu.items.*;
import menu.resources.Circle;
import menu.resources.PointList;
import java.util.Scanner;


public class Main {

    public static Circle circle;
    public static PointList pointList;

    public static void main(String[] args) {

        circle = new Circle(0,0,0);
        pointList = new PointList();

        Scanner scanner = new Scanner(System.in);

        Menu menu = new Menu(scanner, new MenuItem[]{

                new AddNewDot(),
                new ChangeCircleParameters(),
                new ShowDotsInCircle(),
                new ShowDotsOutCircle(),
                new ExitMenu()

        });

        menu.run();

    }
}
